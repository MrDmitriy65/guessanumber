﻿using System;
using System.Windows;

namespace GuessANumber.Model
{
    class MainWindow : Window
    {
        private static MainWindow window = null;

        private MainWindow()
        {

        }

        public static MainWindow GetMainWindow()
        {
            if (window == null)
            {
                window = new MainWindow();
                Console.WriteLine("Create main window");
            }
            return window;
        }
    }
}
