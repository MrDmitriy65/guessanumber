﻿using System;
using System.Windows;
using GuessANumber.Controller;

namespace GuessANumber
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Application app = new Application();
            app.Startup += StartController.StartApp;
            app.Run();
        }
    }
}
