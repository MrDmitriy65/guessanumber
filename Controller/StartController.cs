﻿using System;
using System.Windows;
using GuessANumber.Model;

namespace GuessANumber.Controller
{
    public static class StartController
    {
        public static void StartApp(Object sender, StartupEventArgs e)
        {
            MainWindow window = MainWindow.GetMainWindow();
            configureWindow(window);

        }

        private static void configureWindow(MainWindow w)
        {
            Console.WriteLine("Configurate main window");
            w.Width                 = GLOBALCONST.MAIN_WINDOW_WIDTH;
            w.Height                = GLOBALCONST.MAIN_WINDOW_HEIGHT;
            w.Title                 = GLOBALCONST.APP_TITLE;
            w.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            w.Show();
        }
    }
}
